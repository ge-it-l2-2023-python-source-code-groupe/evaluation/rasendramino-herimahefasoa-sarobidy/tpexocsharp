﻿using System;
using horloge_module;
using cercle;
using client;
using Jeu_de_dee;
namespace TpExoCsharp;

class Program
{
    static void Choix_tp()
    {
        int choix = default;
        while (true)
        {
            while (true)
            {
                Console.WriteLine(@"TP sur : 
    1. Client & Compte class
    2. Circle & Point class
    3. Jeu de Dées
    4. Horloge Module 

    Pour quitter , veuillez entrez '0'
");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    choix = choice;
                    break;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Saisie Éronnée .Valeur numérique attendu .");
                }
            }

            if (choix == 1)
            {
                Deroulement_compte_client.Client();
            }
            else if (choix == 2)
            {
                Deroulement_tp_cercle.Cercle();
            }
            else if (choix == 3)
            {
                Deroulement_du_tp_jeu.Jeu_de_dees();
            }
            else if (choix == 4)
            {
                Déroulement_horloge_module.Menu_Alarme_Horloge();
            }
            else if (choix == 0)
            {
                Console.WriteLine("Merci à Vous d'avoir regarder mon Test . Au revoir 😇 ");
                Console.ReadKey();
                Console.WriteLine();
                break;
            }


            
        }
    }

    

    
    static void Main(string[] args)
    {
        Console.Clear();
        Console.WriteLine(@"       Bien le Bonjour 😇 
    
    Veuillez faire votre choix sur la partie du devoir que vous voulez tester:");
        Choix_tp();
    }
}