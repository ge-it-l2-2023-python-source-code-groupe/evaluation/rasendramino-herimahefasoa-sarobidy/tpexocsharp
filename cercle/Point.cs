using System;

namespace cercle
{
    partial class Point
    {
        public double Y { get; set; }
        public double X { get; set; }


        public Point()
        {
            X = default;
            Y = default;
        }

        public Point(double x, double y) :
        this()
        {
            X=x; Y = y;
        }

        public Point(Point point) :
        this(point.X, point.Y)
        { }

        public void Display()
        {
            Console.WriteLine("Point({0},{1})", X, Y);

        }

    }
}
