using System;

namespace cercle
{
    public class Deroulement_tp_cercle
    {
        public static void Cercle()
        {
            Console.Clear();
            Console.WriteLine("Entrez les coordonnées du centre de votre cercle  :");
            double x = 0, y = 0, r = 0, x1 = 0, y1 = 0;
            bool saisie_invalide = true;
            while (saisie_invalide)
            {
                Console.Write("x:");
                if (double.TryParse(Console.ReadLine(), out double abs))
                {
                    x = abs;
                    saisie_invalide = false;
                }
                else
                {
                    Console.WriteLine("Saisie invalide .Valeur numérique attendue ");
                }
            }
            saisie_invalide = true;
            while (saisie_invalide)
            {
                Console.Write("y:");
                if (double.TryParse(Console.ReadLine(), out double ord))
                {
                    y = ord;
                    saisie_invalide = false;
                }
                else
                {
                    Console.WriteLine("Saisie invalide .Valeur numérique attendue ");
                }
            }


            Point center = new(x, y);
            center.Display();

            saisie_invalide = true;
            while (saisie_invalide)
            {
                Console.Write("\nEntrez le rayon du cercle : ");
                if (double.TryParse(Console.ReadLine(), out double rayon))
                {
                    r = rayon;
                    saisie_invalide = false;
                }
                else
                {
                    Console.WriteLine("Saisie invalide .Valeur numérique attendue ");
                }
            }



            Cercle circle = new(center, r);

            circle.GetPermimeter();
            circle.GetSurface();


            Console.WriteLine("Entrez les coordonnées du point dont vous voulez vérifier l'appartenance au cercle :");
            saisie_invalide = true;
            while (saisie_invalide)
            {
                Console.Write("x:");
                if (double.TryParse(Console.ReadLine(), out double abs))
                {
                    x1 = abs;
                    saisie_invalide = false;
                }
                else
                {
                    Console.WriteLine("Saisie invalide .Valeur numérique attendue ");
                }
            }
            saisie_invalide = true;
            while (saisie_invalide)
            {
                Console.Write("y:");
                if (double.TryParse(Console.ReadLine(), out double ord))
                {
                    y1 = ord;
                    saisie_invalide = false;
                }
                else
                {
                    Console.WriteLine("Saisie invalide .Valeur numérique attendue ");
                }
            }


            Point point = new(x1, y1);
            point.Display();

            circle.IsInclude(point);
            Console.ReadKey();
            Console.Clear();
        }
    }
}