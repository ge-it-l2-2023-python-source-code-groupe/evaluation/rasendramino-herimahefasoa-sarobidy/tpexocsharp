using System;
namespace cercle
{
    partial class Cercle
    {
        public Point Centre { get; set; }
        public double Rayon { get; set; }

        // Utile Pour la Formatation en 2décimale
        readonly string format = "0.00";
        public Cercle(Point C, double r)
        {
            Centre = C; Rayon = r;
        }

        public void GetPermimeter()
        {
            Console.WriteLine("Permimètre :" + (2 * Rayon * Math.PI).ToString(format));
        }

        public void GetSurface()
        {
            Console.WriteLine("Surface :" + (Math.PI * Math.Pow(Rayon, 2)).ToString(format));
        }
        public void IsInclude(Point p)
        {
            double dist = Math.Sqrt(Math.Pow(p.X - Centre.X, 2) + Math.Pow(p.Y - Centre.Y, 2));
            if (dist <= Rayon)
                Console.WriteLine($"Le point ({p.X},{p.Y}) est dans le cercle.");
            else Console.WriteLine($"le point ({p.X},{p.Y}) n'appartient pas au cercle. ");

            double dx = Math.Abs(p.X - Centre.X);
            double dy = Math.Abs(p.Y - Centre.Y);
            Console.WriteLine(dy);

        }

        
    }
}
