namespace client
{
    public class Compte
    {
        public double Solde { get; set; }
        public int Code { get; set; }

        public Client Proprio { get; set; }

        private static int compteur;

        static Compte()
        {
            compteur = 0;
        }
        public Compte()
        {
            Solde = default;
            Code = ++compteur;
        }
        public Compte(Client proprio) :
        this()
        {
            Proprio = proprio;
        }

        public Compte(double solde, Client proprio) :
        this()
        {
            Solde = solde;
        }

        public void Debiter(double montant)
        {
            Solde -= montant;
        }
        public void Debiter(double montant, Compte debiteur)
        {
            Debiter(montant);
            debiteur.Crediter(montant);
        }


        public void Crediter(double montant)
        {
            Solde += montant;
        }
        public void Crediter(double montant, Compte crediteur)
        {
            Crediter(montant);
            crediteur.Debiter(montant);
        }

        public string Resumer()
        {
            return $@"
***************************************
    Numéro de Compte : {Code}
    Solde de compte : {Solde}
    Propriétaire du compte :{Proprio.Afficher()}
***************************************";
        }
        public string NombreDeCompteCrees()
        {
            return $"Le nombre de comptes créés :{compteur}";
        }
    }
}