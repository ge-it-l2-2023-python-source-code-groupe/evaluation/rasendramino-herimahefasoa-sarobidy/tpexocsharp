using System;
namespace client
{
    public class Deroulement_compte_client
    {
        public static void Client()
    {
        Console.Clear();
        string cin; string nom; string prenom; string tel;

        //Création du Premier Compte
        Console.Write("Compte 1:\n Donner le CIN :");
        cin = Console.ReadLine();
        Console.Write("Donner le Nom :");
        nom = Console.ReadLine();
        Console.Write("Donner le Prénom :");
        prenom = Console.ReadLine();
        Console.Write("Donner le numéro de télèphone :");
        tel = Console.ReadLine();

        Compte compte1 = new Compte(new Client(cin, nom, prenom, tel));
        Console.WriteLine($"Détails du compte :{compte1.Resumer()}");

        //Créditation du compte1
        bool saisie_invalide = true;
        double montant = 0;

        while (saisie_invalide)
        {
            Console.Write("Donner le montant à déposer :");


            if (double.TryParse(Console.ReadLine(), out double depot))
            {
                montant = depot;
                saisie_invalide = false;
            }
            else
            {
                Console.WriteLine("Saisie éronnée .Valeur numérique attendu .");
            }

        }


        compte1.Crediter(montant);
        Console.WriteLine($"Opération éfféctuée {compte1.Resumer()}");

        //Débiter le compte 1
        saisie_invalide = true;
        while (saisie_invalide)
        {
            Console.Write("Donner le montant à retirer :");

            if (double.TryParse(Console.ReadLine(), out double retrait))
            {
                montant = retrait;
                saisie_invalide = false;
            }
            else
            {
                Console.WriteLine("Saisie éronnée .Valeur numérique attendu .");
            }

        }



        compte1.Debiter(montant);
        Console.WriteLine($"Opération éfféctuée {compte1.Resumer()}");


        //Création du deuxième compte 
        Console.Write("Compte 2:\n Donner le CIN :");
        cin = Console.ReadLine();
        Console.Write("Donner le Nom :");
        nom = Console.ReadLine();
        Console.Write("Donner le Prénom :");
        prenom = Console.ReadLine();
        Console.Write("Donner le numéro de télèphone :");
        tel = Console.ReadLine();

        Compte compte2 = new(new Client(cin, nom, prenom, tel));
        Console.WriteLine($"Détails du compte :{compte2.Resumer()}");

        //Créditation du compte2 à partir du compte 1




        Console.WriteLine($"Crediter le compte {compte2.Code} à partir du compte {compte1.Code}");
        saisie_invalide = true;
        while (saisie_invalide)
        {
            Console.Write($"Donner le montant à déposer : ");
            if (double.TryParse(Console.ReadLine(), out double depot))
            {
                montant = depot;
                saisie_invalide = false;
            }
            else
            {
                Console.WriteLine("Saisie éronnée .Valeur numérique attendue .");
            }
        }

        compte2.Crediter(montant, compte1);
        Console.WriteLine($"Opération bien effectuée");
        Console.WriteLine($"{compte1.Resumer()} {compte2.Resumer()} ");

        //Débiter le compte1 à partir du compte 2
        Console.WriteLine($"Débiter le compte {compte1.Code} et créditer le compte {compte2.Code}");

        saisie_invalide = true;
        while (saisie_invalide)
        {
            Console.Write($"Donner le montant à retirer: ");
            if (double.TryParse(Console.ReadLine(), out double retrait))
            {
                montant = retrait;
                saisie_invalide = false;
            }
            else
            {
                Console.WriteLine("Saisie éronnée .Valeur numérique attendue .");
            }

        }


        compte1.Debiter(montant, compte2);
        Console.Write($"Opération bien effectuée");


        Console.WriteLine($"{compte1.Resumer()} {compte2.Resumer()} ");



        Console.WriteLine($"{compte2.NombreDeCompteCrees()}");
        Console.ReadKey();
        Console.Clear();        

    }
    }
}