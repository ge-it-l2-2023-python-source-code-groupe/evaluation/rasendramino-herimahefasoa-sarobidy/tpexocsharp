namespace client
{
    public class Client
    {
        public string CIN { get; private set; }
        public string Nom { get; private set; }
        public string Prenom { get; private set; }
        public string Tel { get; private set; }

        // Constructeur Initialisant Le cin , le nom et le prenom
        public  Client(string cin, string nom, string prenom)
        {
            CIN = cin.ToUpper();
            Nom = nom;
            Prenom = prenom;
        }


        // Constructeur Initialisant tous les attributs
        public Client(string cin, string nom, string prenom, string tel) :
    this(cin, nom, prenom)
        {
            Tel = tel;
        }



        public string Afficher()
        {
            return $@"
        CIN: {CIN}
        NOM: {Nom}
        Prénom: {Prenom}
        Tél : {Tel}
        ";
        }


    }
}