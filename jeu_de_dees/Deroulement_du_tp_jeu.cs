using System;
namespace Jeu_de_dee
{
    public class Deroulement_du_tp_jeu
    {

        public static void Jeu_de_dees()
        {
            Console.Clear();
            bool saisie_invalide = true;
            string nom; int nombre_joueurs = 0, choix = default;
            //String pour L'Historique , les Manches et les Noms de Joueurs
            string historique = "", noms_de_joueurs = "";

            Joueur joueur = new();

            //Ajout de Joueurs 
            while (saisie_invalide)
            {
                Console.Write("Entrez le nombre de Joueurs :");
                if (int.TryParse(Console.ReadLine(), out int nJoueurs))
                {
                    nombre_joueurs = nJoueurs;
                    saisie_invalide = false;
                }
                else
                {
                    Console.WriteLine("Saisie éronnée . Valeur numérique attendu .");
                }
            }
            for (int i = 0; i < nombre_joueurs; i++)
            {
                Console.WriteLine($"Entrez le nom du joueurs numero {i + 1}");
                nom = Console.ReadLine() ?? $"Joueur {i + 1}";
                joueur = new Joueur(nom);

                // Ajout des noms de joueurs au string noms_de_joueurs
                if (noms_de_joueurs == "")
                {
                    noms_de_joueurs += nom;
                }
                else
                {
                    noms_de_joueurs += ',' + nom;
                }
            }
            // récupération des noms de joueurs 
            string[] liste_des_joueurs = noms_de_joueurs.Split(',');

            Console.WriteLine("Informations :");

            foreach (string player in liste_des_joueurs)
            {
                joueur.Informations(player);
            }

            bool continuer = true;
            while (continuer)
            {


                bool choix_invalide = true;
                while (choix_invalide)
                {
                    saisie_invalide = true;
                    while (saisie_invalide)
                    {
                        Console.Write(@"
                Veuillez Entrez votre choix :
            1. Nouvelle manche 
            2. Voir l'Historique du Jeu 
            3. Quitter
        
        -->");
                        if (int.TryParse(Console.ReadLine(), out int choice))
                        {
                            choix = choice;
                            saisie_invalide = false;
                        }
                        else
                        {
                            Console.WriteLine("Saisie éronnée. Veullez suivre les instruction \n");
                            continue;
                        }
                    }

                    if (choix == 1 || choix == 2 || choix == 3)
                    {
                        choix_invalide = false;
                    }
                    else
                    {
                        Console.WriteLine("Saisie éronnée. Veullez suivre les instruction \n");
                        continue;
                    }
                }

                if (choix == 1)
                {
                    historique += Historique.Avoir_date_manche();
                    foreach (string player in liste_des_joueurs)
                    {
                        Console.WriteLine(" ---------- x ---------- \n");
                        joueur.Informations(player);

                        Console.WriteLine("Voulez-vous lancer votre dée ? (y/n)");
                        string YorN = Console.ReadLine() ?? "";
                        while (!YorN.ToLower().Equals("y") && !YorN.ToLower().Equals("n"))
                        {
                            Console.WriteLine("Saisie éronnée. Veuillez répondre par 'y' ou 'n'\n");
                            YorN = Console.ReadLine() ?? "";
                        }

                        Console.WriteLine(joueur.Nouvelle_manche(YorN));
                        Console.WriteLine();
                        Console.WriteLine(joueur.Informations(player));

                        //Ajout des manches 
                        historique += joueur.Informations(player);
                    }

                }

                else if (choix == 2)
                {
                    if (historique == "")
                    {
                        Console.WriteLine("Vous n'avez lancee aucune manche jusqu'ici ");
                    }
                    else
                        Console.WriteLine($"{historique}");
                }

                else if (choix == 3)
                {
                    Console.WriteLine("Merci D'être passé 😇");
                    Console.ReadKey();
                    Console.WriteLine("Pimentons un peu le jeu avec un petit pari si vous êtes cap de 😉");
                    Console.ReadKey();
                    Console.Clear();
                    continuer = false;
                }

            }
        }
    }
}