using System;
using client;

namespace Jeu_de_dee
{
    public class Joueur
    {
        public string Nom_joueur { get; set; }
        public static int Point { get; set; }
        public static int Compteur = default;
        public static string[] Face_de_dee = {"|   |\n| * |\n|   |","|*  |\n|   |\n|  *|","|*  |\n| * |\n|  *|","|* *|\n|   |\n|* *|","|* *|\n| * |\n|* *|","|* *|\n|* *|\n|* *|"};

        public Joueur() { }

        public Joueur(string nomJoueur)
        {
            this.Nom_joueur = nomJoueur;
        }

        public string Informations(string nom)
        {
            return $"- {nom}: {Point} point(s)";
        }
        public static int Tirage()
        {
            Random rand = new();
            return rand.Next(1, 7);
        }


        public static void Ajout_de_point()
        {
            int tirage = Tirage();
            if (tirage == 6)
            {
                Point += 2;
                Compteur += 1;
                if (Compteur > 1)
                {
                    Point += 1;
                }
            }
            else Point = 0;
        }

        public String Nouvelle_manche(string choix)
        {
            Console.Clear();
            Console.WriteLine("Nouvelle Manche en Cours ...\n");
            if (choix == "y")
            {

                int tirage = Tirage();
                Ajout_de_point();
                return $" {Face_de_dee[tirage - 1]}";
            }

            return "Vous avez passé votre tour ...";



        }
    }
}
