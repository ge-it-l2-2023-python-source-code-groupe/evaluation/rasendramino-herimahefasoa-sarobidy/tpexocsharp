using System;


namespace horloge_module
{
    public class Alarme
    {
        public string Nom { get; set; }
        public string Heure { get; set; }
        public string Date { get; set; }
        public bool Lancer_une_fois { get; set; }
        public bool Periodique { get; set; }
        public bool Sonnerie_par_defaut { get; set; }


        public Alarme(string nom, string heure, string date, bool lancement, bool periodique, bool sonnerie)
        {
            this.Nom = nom;
            this.Date = date;
            this.Heure = heure;
            this.Lancer_une_fois = lancement;
            this.Periodique = periodique;
            this.Sonnerie_par_defaut = sonnerie;
        }

        public override string ToString()
        {
            return $"{Nom} - {Heure} {(Periodique ? "(périodique)" : "")}\n{Date}\n";
        }

    }
}