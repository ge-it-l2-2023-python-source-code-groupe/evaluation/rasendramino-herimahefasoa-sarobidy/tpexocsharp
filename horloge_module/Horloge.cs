using System;
namespace horloge_module
{
    class Horloge
    {
        public string Ville { get; set; }

        public Horloge(string ville)
        {
            Ville = ville;
        }

        public override string ToString()
        {
            return $"{Ville} - {DateTime.UtcNow.AddHours(GetTimeZone(Ville)):HH:mm}\n{(Ville == "ANTANANARIVO" ? DateTime.UtcNow.ToString("ddd dd MMM") : "")}\n";
        }

        private int GetTimeZone(string ville)
        {
            switch (ville)
            {
                case "Moscou":
                    return 3;
                case "Dubaï":
                    return 4;
                case "Mexique":
                    return -6;
                default:
                    return 0;
            }
        }

        
    }

}
