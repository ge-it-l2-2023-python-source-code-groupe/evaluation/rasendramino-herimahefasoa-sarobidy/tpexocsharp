using System;
using System.Collections.Generic;
namespace horloge_module
{
    public class Déroulement_horloge_module
    {

        public static void Menu_Alarme_Horloge()
        {
            List<Horloge> horloges = new();
            List<Alarme> alarmes = new List<Alarme>();

            int choix_partie = default;

            Console.WriteLine("Bienvenu ddans le Menu principale 😇");
            while (true)
            {
                while (true)
                {
                    Console.Write(@" 
            
                Choisissez une option :
                1. Alarmes
                2. Horloges
                3. Quitter ");
                    Console.Write(@"Entrez votre choix 
-->");
                    if (int.TryParse(Console.ReadLine(), out int choice))
                    {
                        choix_partie = choice;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Saisie Invalide . Veuillez suivre les instructions...");
                    }
                    break;
                }
                if (choix_partie == 1)
                {
                    Alarmes(alarmes);
                }
                if (choix_partie == 2)
                {
                    Horloges(horloges);
                }
                if (choix_partie == 3)
                {
                    Console.WriteLine("Merci de votre visite 😇");
                    break;
                }
                else
                {
                    Console.WriteLine("Saisie Éronnée . Suiveez les instructions");
                }


            }

        }

        /* ----------------------------------- x ALARMES x -----------------------------------     */
        static void Alarmes(List<Alarme> alarmes)
        {
            int choix_partie_alarme = default;
            while (true)
            {
                while (true)
                {
                    Console.WriteLine(@"Choisissez une option :
                1. Voir les Alarmes Actives
                2. Ajouter une Alarme
                3. Quitter Alarme ");
                    Console.WriteLine(@"Entrez votre choix 
    -->");
                    if (int.TryParse(Console.ReadLine(), out int choice))
                    {
                        choix_partie_alarme = choice;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Saisie Invalide . Veuillez suivre les instructions...");
                    }
                    break;

                }
                if (choix_partie_alarme == 1)
                {
                    Afficher_alarme_actives(alarmes);
                }
                if (choix_partie_alarme == 2)
                {
                    Ajouter_nouvelles_alarmes(alarmes);
                }
                if (choix_partie_alarme == 3)
                {
                    Console.WriteLine("Bye Alors 😉");
                    break;
                }
                else
                {
                    Console.WriteLine("Saisie Éronnée . Suiveez les instructions");
                }

            }
        }
        static void Afficher_alarme_actives(List<Alarme> alarmes)
        {
            Console.WriteLine("Listes des Alarmes Actives ");
            if (alarmes.Count == 0)
            {
                Console.WriteLine("Aucune Alarme active .");
                Console.ReadKey();
            }
            else
            {
                foreach (var alarme in alarmes)
                {
                    Console.WriteLine(alarme);
                }
                Console.WriteLine($"Nombres d'alarmes : {alarmes.Count}");
            }
        }
        static void Ajouter_nouvelles_alarmes(List<Alarme> alarmes)
        {
            Console.WriteLine("Completez les champs suivants pour la nouvelle alarme ");
            Console.Write("Entrez le nom de l'Alarme : ");
            string nom_nouvelle_alarme = Console.ReadLine() ?? "Nouvelle alarme ";
            int hh = default, mm = default;
            Console.WriteLine("Entrez l'heure de votre alarme :");
            while (true)
            {
                while (true)
                {
                    Console.Write(" hh :");
                    if (int.TryParse(Console.ReadLine(), out int h))
                    {
                        hh = h; break;
                    }
                    else
                    {
                        Console.WriteLine("saisie éronnée . Valeur de type numérique attendu ...");
                    }
                }
                while (true)
                {
                    Console.Write(" mm :");
                    if (int.TryParse(Console.ReadLine(), out int min))
                    {
                        mm = min; break;
                    }
                    else
                    {
                        Console.WriteLine("saisie éronnée . Valeur de type numérique attendu ...");
                    }
                }
                if ((0 <= hh && hh < 24) || (0 <= mm && mm < 60))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Veuillez entree une heure valide");
                }
            }
            string heure_alarme = hh + ":" + mm;
            Console.Write("Date de planification (L/M/ME/J/V/S/D): ");
            string date_alarme = Console.ReadLine() ?? "L/M/ME/J/V/S/D";
            Console.Write("Lancer une seule fois (y/n): ");
            bool lancer_une_seule_fois = Console.ReadLine().ToLower() == "y";
            Console.Write("Périodique (y/n): ");
            bool periodique = Console.ReadLine().ToLower() == "y";
            Console.Write("Activer sonnerie par défaut (y/n): ");
            bool sonnerie_par_defaut = Console.ReadLine().ToLower() == "y";
            Alarme nouvelle_alarme = new Alarme(nom_nouvelle_alarme, heure_alarme, date_alarme, lancer_une_seule_fois, periodique, sonnerie_par_defaut);
            // Ajout de la nouvelle alarme à la liste des alarmes
            alarmes.Add(nouvelle_alarme);

            Console.WriteLine("Nouvelle alarme ajoutée ");
            Console.ReadKey();

        }

        /* ----------------------------------- x  HORLOGES x ----------------------------------- */
        static void Horloges(List<Horloge> horloges)
        {
            int choix_partie_horloge = default;
            while (true)
            {
                while (true)
                {
                    Console.WriteLine(@"Choisissez une option :
                1. Voir les Horloges Actives
                2. Ajouter une Horloge
                3. Quitter l'Horloge");
                    Console.WriteLine(@"Entrez votre choix 
    -->");
                    if (int.TryParse(Console.ReadLine(), out int choice))
                    {
                        choix_partie_horloge = choice;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Saisie Invalide . Veuillez suivre les instructions...");
                    }
                    break;

                }
                if (choix_partie_horloge == 1)
                {
                    Afficher_horloges_actives(horloges);
                }
                if (choix_partie_horloge == 2)
                {
                    Ajouter_nouvelles_horloges(horloges);
                }
                if (choix_partie_horloge == 3)
                {
                    Console.WriteLine("Bye Alors 😉");
                    break;
                }
                else
                {
                    Console.WriteLine("Saisie Éronnée . Suiveez les instructions");
                }

            }
        }

        static void Afficher_horloges_actives(List<Horloge> horloges)
        {
            Console.WriteLine("Listes des Horloges Actives ");
            if (horloges.Count == 0)
            {
                Console.WriteLine("Aucune Horloge active .");
                Console.ReadKey();
            }
            else
            {
                foreach (var horloge in horloges)
                {
                    Console.WriteLine(horloge);
                }
            }
        }
        static void Ajouter_nouvelles_horloges(List<Horloge> horloges)
        {
            Console.Write("Choisissez une ville [Moscou, Dubaï, Mexique]: ");
            string ville = Console.ReadLine();

            Horloge nouvelle_horloge = new(ville);
            // Ajout de la nouvelle nouvelle à la lise des horloges 
            horloges.Add(nouvelle_horloge);

            Console.WriteLine("Votre nouvelle horloge a bien été enregistrée ");
        }


    }
}